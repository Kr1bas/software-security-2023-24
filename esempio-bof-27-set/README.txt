Per installare la libreria pwntools:
	https://docs.pwntools.com/en/stable/install.html
Per compilare il codice rimuovendo le protezioni utilizzare il seguente comando:
	gcc -no-pie -m32 -mpreferred-stack-boundary=2 -fno-stack-protector -zexecstack example.c
Per rendere eseguibile il binario o lo script python utilizzare il comando:
	chmod +x <nome_file>
